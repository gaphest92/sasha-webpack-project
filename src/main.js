import '@splidejs/splide/dist/css/splide.min.css';
import '@splidejs/splide/dist/css/themes/splide-sea-green.min.css';
import Splide from '@splidejs/splide';

function main() {

    new Splide( '.splide', {
        type   : 'loop',
        focus  : 'center',
        padding: {
            right: '10rem',
            left : '10rem',
        },
        autoplay: true,
        fixedHeight: '360px',
        fixedWidth: '260px',
        gap: '20px'
    } ).mount();
}

export default main;